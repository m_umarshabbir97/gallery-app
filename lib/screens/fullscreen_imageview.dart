import 'package:flutter/material.dart';
import 'package:full_screen_image_null_safe/full_screen_image_null_safe.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:transparent_image/transparent_image.dart';

class FullScreenImageView extends StatelessWidget {
  final Medium medium;
  const FullScreenImageView(this.medium, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FullScreenWidget(
        child: Hero(
      tag: medium.id,
      child: FadeInImage(
          fit: BoxFit.contain,
          placeholder: MemoryImage(kTransparentImage),
          image: PhotoProvider(
            mediumId: medium.id,
          ),
        ),
      
    ));
  }
}
