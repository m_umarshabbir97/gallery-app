import 'package:photo_gallery/photo_gallery.dart';

Future<List<Album>> getAlbums() async {
  final List<Album> imageAlbums = await PhotoGallery.listAlbums(
    mediumType: MediumType.image,
  );
  return imageAlbums;
}

Future<List<Medium>> getImages(Album album) async {
  final MediaPage imagePage = await album.listMedia();
  return imagePage.items;
}
