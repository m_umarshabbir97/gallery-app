import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:gallery/repositories/images_repository.dart';
import 'package:gallery/screens/fullscreen_imageview.dart';
import 'package:gallery/ui/widgets.dart';
import 'package:gallery/utils/permissions.dart';
import 'package:photo_gallery/photo_gallery.dart';

class GalleryScreen extends StatefulWidget {
  final Album album;
  const GalleryScreen(this.album, {Key? key}) : super(key: key);

  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  List<Medium> images = [];
  var pressed = -1;
  var offset = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPermission();
  }

  void fetchImages() async {
    images.addAll(await getImages(widget.album));
    offset += 20;
    setState(() {});
    print("Total Imaegs ${images.length}");
  }

  void getPermission() async {
    if (await requestPermission()) {
      fetchImages();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${widget.album.name} (${widget.album.count})"),
      ),
      body: StaggeredGridView.countBuilder(
          crossAxisCount: 3,
          itemCount: images.length,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => FullScreenImageView(images[index])));
              },
              onTapUp: (_) {
                setState(() {
                  pressed = -1;
                });
              },
              onTapDown: (_) {
                setState(() {
                  pressed = index;
                });
              },
              child: pressed == index
                  ? Hero(
                      tag: images[index].id,
                      child: getGalleryWidget(images[index], true))
                  : Hero(
                      tag: images[index].id,
                      child: getGalleryWidget(images[index], false))),
          staggeredTileBuilder: (index) =>
              StaggeredTile.count(1, index % 7 == 0 ? 2 : 1)),
    );
  }
}
