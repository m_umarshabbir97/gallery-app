import 'package:flutter/material.dart';
import 'package:photo_gallery/photo_gallery.dart';
import 'package:transparent_image/transparent_image.dart';

Widget getAlbumWidget(Album album, bool pressed) {
  print("Pressed $pressed");
  return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
    Expanded(
        child: Container(
          padding: pressed?EdgeInsets.all(5):EdgeInsets.all(0),

      width: double.infinity,
      height: double.infinity,
      child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          clipBehavior: Clip.hardEdge,
          child: pressed
              ? ColorFiltered(
                  child: getAlbumThumb(album),
                  colorFilter: ColorFilter.mode(Colors.grey, BlendMode.color),
                )
              : getAlbumThumb(album),
    ))),
    SizedBox(
        height: 20,
        width: double.infinity,
        child: Text(
          album.name,
          overflow: TextOverflow.clip,
          textAlign: TextAlign.center,
          maxLines: 1,
          style: TextStyle(
              color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
        ))
  ]);
}

Widget getGalleryWidget(Medium image, bool pressed) {
  return Container(
    width: double.infinity,
    height: double.infinity,
          padding: pressed?EdgeInsets.all(5):EdgeInsets.all(0),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        clipBehavior: Clip.hardEdge,
        child: pressed
            ? ColorFiltered(
                child: getMediumThumb(image),
                colorFilter: ColorFilter.mode(Colors.grey, BlendMode.color),
              )
            : getMediumThumb(image)),
  );
}

Widget getAlbumThumb(Album album) {
  return FadeInImage(
    fit: BoxFit.cover,
    placeholder: MemoryImage(kTransparentImage),
    image: AlbumThumbnailProvider(
      albumId: album.id,
      mediumType: MediumType.image,
      width: 128,
      height: 128,
      highQuality: true
    ),
  );
}

Widget getMediumThumb(Medium medium) {
  return FadeInImage(
    fit: BoxFit.cover,
    placeholder: MemoryImage(kTransparentImage),
    image: ThumbnailProvider(
      mediumId: medium.id,
      mediumType: MediumType.image,
      width: 256,
      height: 256,
      highQuality: true
    ),
  );
}
