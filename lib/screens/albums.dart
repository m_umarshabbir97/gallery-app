import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:gallery/repositories/images_repository.dart';
import 'package:gallery/screens/gallery.dart';
import 'package:gallery/ui/widgets.dart';
import 'package:gallery/utils/permissions.dart';
import 'package:photo_gallery/photo_gallery.dart';

class AlbumsScreen extends StatefulWidget {
  const AlbumsScreen({Key? key}) : super(key: key);

  @override
  _AlbumsScreenState createState() => _AlbumsScreenState();
}

class _AlbumsScreenState extends State<AlbumsScreen> {
  List<Album> albums = [];
  var pressed = -1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPermission();
  }

  void initAlbums() async {
      albums.addAll(await getAlbums());
      albums.sort((a, b) {
        return a.name.toLowerCase().compareTo(b.name.toLowerCase());
      });
      setState(() {});
      print("Total Albums ${albums.length}");
    
  }

  void getPermission() async {
    if (await requestPermission()) {
      initAlbums();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Albums"),
      ),
      body: StaggeredGridView.countBuilder(
          crossAxisCount: 3,
          itemCount: albums.length,
          mainAxisSpacing: 5,
          crossAxisSpacing: 5,
          itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => GalleryScreen(albums[index])));
              },
              onTapUp: (_) {
                setState(() {
                  pressed = -1;
                });
              },
              onTapDown: (_) {
                setState(() {
                  pressed = index;
                });
              },
              child: pressed == index
                  ? getAlbumWidget(albums[index], true)
                  : getAlbumWidget(albums[index], false)),
          staggeredTileBuilder: (index) => StaggeredTile.count(1, 1)),
    );
  }
}
